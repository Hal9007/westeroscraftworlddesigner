#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QDesktopWidget>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Sidebar label setup
    QFont cinzelFont("Cinzel", 16, QFont::Black);
    cinzelFont.setStyleStrategy(QFont::PreferAntialias);

    ui->sidebarLabel->setStyleSheet("color: #BE943F;"
                                    "margin-top: 10px;"
                                    "margin-bottom: 10px;");
    ui->sidebarLabel->setFont(cinzelFont);
    ui->sidebarLabel->setAlignment(Qt::AlignHCenter);
    ui->sidebarLabel->setText("WesterosCraft\nWorld  Designer");

    connect(ui->treeTabWidget, SIGNAL(currentChanged(int)), this, SLOT(setButtonWidget(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setButtonWidget(int index)
{
    ui->editStack->setCurrentIndex(index);
}

void MainWindow::showEvent(QShowEvent *event)
{
    Q_UNUSED(event);

    centerOnScreen();
}

void MainWindow::centerOnScreen()
{
    QDesktopWidget screen;

    QRect screenGeom = screen.screenGeometry(this);

    int screenCenterX = screenGeom.center().x();
    int screenCenterY = screenGeom.center().y();

    move(screenCenterX - width () / 2,
         screenCenterY - height() / 2);
}
