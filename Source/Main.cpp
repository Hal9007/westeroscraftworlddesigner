#include "MainWindow.h"

#include <QApplication>
#include <QStyleFactory>
#include <QStringList>
#include <QMessageBox>
#include <QFontDatabase>
#include <QFont>
#include <QFile>

#include <iostream>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    // Application icon on Unix systems
    #ifndef Q_OS_WIN
        app.setWindowIcon(QIcon(":/default/Icons/world_designer_icon.ico"));
    #endif

    // Set Fusion app style with dark theme
    app.setStyle(QStyleFactory::create("Fusion"));
    QPalette darkPalette;
    darkPalette.setColor(QPalette::Window, QColor(53,53,53));
    darkPalette.setColor(QPalette::WindowText, Qt::white);
    darkPalette.setColor(QPalette::Base, QColor(25,25,25));
    darkPalette.setColor(QPalette::AlternateBase, QColor(53,53,53));
    darkPalette.setColor(QPalette::ToolTipBase, Qt::white);
    darkPalette.setColor(QPalette::ToolTipText, Qt::white);
    darkPalette.setColor(QPalette::Text, Qt::white);
    darkPalette.setColor(QPalette::Button, QColor(53,53,53));
    darkPalette.setColor(QPalette::ButtonText, Qt::white);
    darkPalette.setColor(QPalette::BrightText, Qt::red);
    darkPalette.setColor(QPalette::Link, QColor(42, 130, 218));

    darkPalette.setColor(QPalette::Highlight, QColor(218, 156, 42));
    darkPalette.setColor(QPalette::HighlightedText, Qt::black);

    app.setPalette(darkPalette);

    // Set StyleSheet for app
    app.setStyleSheet("QToolTip { color: #ffffff; background-color: #2a82da; border: 1px solid white; }");

    // Main font rendering
    QFont mainFont = app.font();
    mainFont.setStyleStrategy(QFont::PreferAntialias);
    app.setFont(mainFont);

    // Dynamically load custom fonts
    QStringList list;
    list << "Cinzel-Black.otf" << "Cinzel-Bold.otf" << "Cinzel-Regular.otf";

    int fontID(-1);
    bool fontWarningShown(false);
    for (auto font : list)
    {
        QFile res(":/Typeface/Fonts/" + font);
        if (!res.open(QIODevice::ReadOnly))
        {
            if (!fontWarningShown)
            {
                QMessageBox::warning(0, "Application", (QString) "Warning: Unable to load font " + QChar(0x00AB) + font + QChar(0x00BB) + ".");
                fontWarningShown = true;
            }
        }
        else
        {
            fontID = QFontDatabase::addApplicationFontFromData(res.readAll());
            if (fontID == -1 && !fontWarningShown)
            {
                QMessageBox::warning(0, "Application", (QString) "Warning: Unable to load font " + QChar(0x00AB) + font + QChar(0x00BB) + ".");
                fontWarningShown = true;
            }
        }
    }

    MainWindow mainWin;
    mainWin.show();

    return app.exec();
}
