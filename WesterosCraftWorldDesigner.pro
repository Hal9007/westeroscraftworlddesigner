#-------------------------------------------------
#
# Project created by QtCreator 2015-06-30T00:59:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = WesterosCraftWorldDesigner
TEMPLATE = app

RC_FILE = Resource/win_icon.rc

SOURCES +=\
    Source/Main.cpp \
    Source/MainWindow.cpp

HEADERS  += \
    Source/MainWindow.h

FORMS    += \
    Resource/UI/MainWindow.ui

INCLUDEPATH += Source/

RESOURCES += \
    Resource/Icons.qrc \
    Resource/Fonts.qrc
